const express = require ("express");
const data = require("../models/posts");

const postRouter = express.Router();



/**
 * @swagger
 * /posts/findall:
 *   get:
 *     summary: Returns all posts
 *     tags: [Posts]
 *     responses:
 *       200:
 *         description: the list of the posts
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Post'
 */
 postRouter.get('/findall', function(req, res) {
    data.find(function(err, data) {
        if(err){
            console.log(err);
        }
        else{
            res.send(data);
        }
    });  
 });
/**
 * @swagger
 * /posts/{id}:
 *   get:
 *     summary: gets posts by id
 *     tags: [Posts]
 *     parameters:
 *       - in : path
 *         name: id
 *         description: id of post
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: posts by its id
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Post'
 *       400:
 *         description: post can not be found
 */



 postRouter.get('/:id', function(req, res) {
    data.findById((req.params.id),function(err, data) {
        if(err){
            console.log(err);
        }
        else{
            res.send(data);
        }
    });  
 });


/**
 * @swagger
 * /posts/data:
 *   post:
 *     summary: Create a new book
 *     tags: [Posts]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Post'
 *     responses:
 *       200:
 *         description: The post was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Post'
 *       500:
 *         description: Some server error
 */
postRouter.post('/data', function(req, res) {
    const post = new data();
       post.Id = req.body.Id;
       post.userId = req.body.userId;
       post.title = req.body.title;
       post.body = req.body.body;
       
       post.save(function(err, data){
           if(err){
               console.log(error);
           }
           else{
               res.send("Data inserted");
           }
       });
    });
 

// postRouter.post("/data", (req, res) => {
//   try {
//     const post = {
//       id: data.length + 1,...req.body
//     };
//     const dataSave=data.save(post);
//     res.send(dataSave);
//   } catch (error) {
//     return res.status(500).send(error);
//   }
// });

/**
 * @swagger
 * /posts/{id}:
 *   put:
 *     summary: updates posts by id
 *     tags: [Posts]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: integer
 *         required: true
 *         description: post id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Post'
 *     responses:
 *       200:
 *         decsription: The post was updated
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Post'
 *       404:
 *         description: post was not found.
 *       500:
 *         description: Some errors happend.
 *
 */
 postRouter.put('/:id', function(req, res) {
    data.findByIdAndUpdate(req.params.id, 
    {title:req.body.title},  {body:req.body.body}, function(err, data) {
        if(err){
            console.log(err);
        }
        else{
            res.send("data updated");
            console.log("Data updated!");
        }
    });  
});


/**
 * @swagger
 *  /posts/{id}:
 *    delete:
 *      summary: removes post from array
 *      tags: [Posts]
 *      parameters:
 *        - in: path
 *          name: id
 *          description: post id
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        200:
 *          description: The post was deleted
 *        404:
 *          description: The post was not found
 *
 */
 postRouter.delete('/:id', function(req, res) {
    data.findByIdAndDelete((req.params.id),  
    function(err, data) {
        if(err){
            console.log(err);
        }
        else{
            res.send("data are deleted");
        }
    });  
});

module.exports = postRouter;
